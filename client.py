# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 17:54:44 2021

@author: Lucy
"""

import time
import urllib.parse
from typing import optional, dict, any, list

from requests import request, session, response
import hmac
from datetime import datetime
import pandas as pd
from pypfopt.efficient_frontier import efficientfrontier
from pypfopt.expected_returns import mean_historical_return
from pypfopt.risk_models import covarianceshrinkage
from pypfopt import plotting



class ftxclient:
    _ENDPOINT = 'https://ftx.com/api/'

    def __init__(self, api_key=None, api_secret=None, subaccount_name=None) -> None:
        self._session = Session()
        self._api_key = api_key
        self._api_secret = api_secret
        self._subaccount_name = subaccount_name

    def _get(self, path: str, params: Optional[Dict[str, Any]] = None) -> Any:
        return self._request('GET', path, params=params)

    def _post(self, path: str, params: Optional[Dict[str, Any]] = None) -> Any:
        return self._request('POST', path, json=params)

    def _delete(self, path: str, params: Optional[Dict[str, Any]] = None) -> Any:
        return self._request('DELETE', path, json=params)

    def _request(self, method: str, path: str, **kwargs) -> Any:
        request = Request(method, self._ENDPOINT + path, **kwargs)
        self._sign_request(request)
        response = self._session.send(request.prepare())
        return self._process_response(response)

    def _sign_request(self, request: Request) -> None:
        ts = int(time.time() * 1000)
        prepared = request.prepare()
        signature_payload = f'{ts}{prepared.method}{prepared.path_url}'.encode()
        if prepared.body:
            signature_payload += prepared.body
        signature = hmac.new(self._api_secret.encode(), signature_payload, 'sha256').hexdigest()
        request.headers['FTX-KEY'] = self._api_key
        request.headers['FTX-SIGN'] = signature
        request.headers['FTX-TS'] = str(ts)
        if self._subaccount_name:
            request.headers['FTX-SUBACCOUNT'] = urllib.parse.quote(self._subaccount_name)

    def _process_response(self, response: Response) -> Any:
        try:
            data = response.json()
        except ValueError:
            response.raise_for_status()
            raise
        else:
            if not data['success']:
                raise Exception(data['error'])
            return data['result']

    def list_futures(self) -> List[dict]:
        return self._get('futures')

    def list_markets(self) -> List[dict]:
        return self._get('markets')

    def get_orderbook(self, market: str, depth: int = None) -> dict:
        return self._get(f'markets/{market}/orderbook', {'depth': depth})

    def get_trades(self, market: str) -> dict:
        return self._get(f'markets/{market}/trades')
    
    def get_hist_prices(self, market: str, resolution: int, start_time: float, end_time: float) -> List:
        """
        resolution: window length in seconds. options: 15, 60, 300, 900, 3600,
                    14400, 86400, or any multiple of 86400 up to 30*86400
        
        """
        response = self._get(f'markets/{market}/candles?resolution={resolution}&start_time={start_time}&end_time={end_time}')
        print(f'fetching {len(response)} records of {market} from {start_time} to {end_time}')
        
        return response
    

if __name__ == "__main__":
    # Config
    my_api = 'YOUR_API'
    my_secret = 'YOUR_SECRET_KEY'
    start = datetime.strptime("2021-10-01T00:00:00+00:00", "%Y-%m-%dT%H:%M:%S%z").timestamp()
    end = datetime.strptime("2021-10-31T23:00:00+00:00", "%Y-%m-%dT%H:%M:%S%z").timestamp()
    resolution = "3600"
    output_path = "./"
    
    
    # Download data
    my_client = FtxClient(my_api, my_secret)
    futures = my_client.list_futures()
    valid_futures = [f['name'] for f in futures if f["type"] == "perpetual" and f["underlying"] in ("BTC", "ETH", "ADA") ]
    fut_prices = {}
    for fut_name in valid_futures:
        prices = my_client.get_hist_prices(fut_name, int(resolution), start, end)
        prices_df = pd.DataFrame(prices)
        prices_df["startTime"] = pd.to_datetime(prices_df["startTime"])
        prices_df.set_index("startTime", inplace=True)
        fut_prices[fut_name] = prices_df
        # Output raw data
        prices_df.to_csv(output_path + fut_name + ".csv")
        
   
    # Process and Analyze
    prices = []
    for fut_name, prices_df in fut_prices.items():
        price = prices_df["close"]
        price.name = fut_name
        prices.append(price)
        
    prices_join = pd.concat(prices, axis=1)
    
    # Mean-Variance    
    mu = mean_historical_return(prices_join)  #return
    S = CovarianceShrinkage(prices_join).ledoit_wolf() #var-cov matrix
    ef = EfficientFrontier(mu, S)
    weights = ef.max_sharpe()
    ef.save_weights_to_file(output_path + "weights.txt")  # saves to file
    
    # Plotting
    plotting.plot_efficient_frontier(ef, show_assets=True, showfig=True)
    
        
    