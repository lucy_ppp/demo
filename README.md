# Mean Variance portfolio

## How to run?
1. install requirements.txt
2. change parameter in python file
3. run by `python client.py`

---

## How does it work?
1. it uses rest api of FTX exchange to get 1hour data for Oct 2021 of BTC, ETH, ADA
2. use python package to calculate efficient froniter of mean-variance theory


